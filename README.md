# ion-container

[![Docker Repository on Quay](https://quay.io/repository/dontlaugh/ion/status "Docker Repository on Quay")](https://quay.io/repository/dontlaugh/ion)

Docker container builds for the ion shell

Right now we simply should be able to build with the official package repo
setup by mmstick

Against Ubuntu 18.04 (broken?)

```
docker build --pull -t quay.io/dontlaugh/ion:ubuntu-18.04-packaged -f Dockerfile.ubuntu-18.04-packaged .
```

Against Ubuntu 19.04

```
docker build --pull -t quay.io/dontlaugh/ion:ubuntu-19.04-packaged -f Dockerfile.ubuntu-19.04-packaged .
```

